class ExceptionDataFrameEmpty(Exception):
    def __init__(self, code, message):
        self.code = code
        self.message = message
        super().__init__(code, message)
