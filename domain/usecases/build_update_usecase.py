import pandas as pd
from infra.entities import CoveredItemEntity
from datetime import datetime

from tools import check_type_dt, get_description_field_int, get_description_field_date, get_description_field


class BuildUpdateUseCase:
    def __init__(self, data_frame_register, id_structure, id_client, date_contract, data_frame_covered_item,
                 date_format: str, structure_data_frame):
        self.data_frame_register = data_frame_register
        self.id_structure = id_structure
        self.id_client = id_client
        self.date_contract = date_contract
        self.data_frame_covered_item = data_frame_covered_item
        self.date_format = date_format
        self.structure_data_frame = structure_data_frame
        self.field_structure = None

    def build_update(self):
        entities = []
        self._process(self.data_frame_register, entities)
        return entities

    def _process(self, data_frame, entities):
        self.field_structure = self.structure_data_frame.query('tabelaDestino == "ITEMCOBERTO"')
        item_coberto_data_base = pd.merge(left=data_frame, right=self.data_frame_covered_item,
                                          right_on=['apolice', 'apoliceitem', 'numerocartao'],
                                          left_on=['APOLICE', 'APOLICEITEM', 'NUMEROCARTAO'],
                                          how='inner')
        for index, row in item_coberto_data_base.iterrows():
            data_lote = get_description_field_date(self.field_structure, 'DATA_LOTE', row, self.date_format)
            codigosusep = get_description_field_int(self.field_structure, 'CODIGOSUSEP', row)
            id_sucursal = get_description_field_int(self.field_structure, 'ID_SUCURSAL', row)

            # Valida se o plano está passível de alteração

            if len(item_coberto_data_base) > 0:
                if row.PLANO != str(item_coberto_data_base.id_plano.values[0]):
                    date_start_validity = row.DATAINICIOVIGENCIA
                    today = datetime.today().date()
                    if date_start_validity <= today:
                        if date_start_validity >= check_type_dt(item_coberto_data_base.datainiciovigencia):
                            datafimvigencia = row.DATAFIMVIGENCIA
                            datafimcobranca = None
                            # Validar endosso fim de vigencia
                            if datafimvigencia > row.datafimvigencia.date() or (
                                    datafimvigencia < row.datafimvigencia.date() and (
                                    datafimvigencia > today)):
                                datafimcobranca = row.DATAFIMVIGENCIA

                            elif datafimvigencia < row.datafimvigencia.date() and (
                                    datafimvigencia <= today <= row.datafimvigencia.date()
                            ):
                                datafimcobranca = today
                            elif datafimvigencia < row.datafimvigencia.date() < today:
                                datafimcobranca = row.datafimvigencia.date()
                            entities = self._build_update(data_frame=data_frame, canceled_old_register=True,
                                                          item_coberto_data_base=item_coberto_data_base, row=row,
                                                          data_lote=data_lote, codigosusep=codigosusep,
                                                          id_sucursal=id_sucursal, datafimcobranca=datafimcobranca,
                                                          entities=entities, datafimvigencia=datafimvigencia)
                        else:
                            message = '[007] Data de Inicio informada é menor que a Data de Inicio' \
                                      ' existente no registro na USS'
                            data_frame.at[index, 'MESSAGE_ERROR'] = message
                    else:
                        message = '[016] Data de inicio de vigência enviada é maior que data de processamento'
                        data_frame.at[index, 'MESSAGE_ERROR'] = message
                else:
                    entities = self._build_update(data_frame=data_frame, canceled_old_register=False,
                                                  item_coberto_data_base=item_coberto_data_base, row=row,
                                                  data_lote=data_lote, codigosusep=codigosusep,
                                                  id_sucursal=id_sucursal,
                                                  datafimcobranca=row.datafimcobranca.date(),
                                                  entities=entities, datafimvigencia=row.DATAFIMVIGENCIA)
            else:
                data_frame.at[index, 'JOB_TO_EXECUTE'] = 'I'

    def _build_update(self, data_frame, canceled_old_register, item_coberto_data_base, row, data_lote, codigosusep,
                      id_sucursal,
                      datafimcobranca, entities, datafimvigencia):
        field_structure = self.structure_data_frame.query('tabelaDestino == "ITEMCOBERTO"')
        lote = get_description_field_int(field_structure, 'LOTE', row)
        if canceled_old_register:
            entities.append(CoveredItemEntity(id_itemcoberto=row.id_itemcoberto_y,
                                              id_plano=row.id_plano,
                                              id_statusitemcoberto=3,
                                              apolice=row.apolice,
                                              datainiciovigencia=
                                              row.datainiciovigencia,
                                              apoliceitem=str(row.apoliceitem),
                                              datafimvigencia=row.datafimvigencia,
                                              numerocartao=row.numerocartao,
                                              id_estrutura=int(row.id_estrutura),
                                              matricula=row.matricula,
                                              lote=row.lote,
                                              blanket=row.blanket,
                                              dataprocessamento=row.dataprocessamento,
                                              id_clientecorporativo=row.id_clientecorporativo,
                                              datacadastro=row.datacadastro,
                                              datacancelamento=row.datacancelamento,
                                              datainiciocobranca=row.datainiciocobranca,
                                              datafimcobranca=row.datafimcobranca,
                                              qtddiasendosso=row.qtddiasendosso,
                                              id_statusprovisorio=row.id_statusprovisorio,
                                              datastatusprovisorio=row.datastatusprovisorio,
                                              datareativacao=row.datareativacao,
                                              data_lote=row.data_lote,
                                              data_expurgo=row.data_expurgo,
                                              codigosusep=row.codigosusep,
                                              id_sucursal=row.id_sucursal))

            entities.append(CoveredItemEntity(id_plano=int(row.PLANO),
                                              id_statusitemcoberto=int(row.OPERACAO),
                                              apolice=row.APOLICE,
                                              datainiciovigencia=row.DATAINICIOVIGENCIA,
                                              apoliceitem=row.APOLICEITEM,
                                              datafimvigencia=datafimvigencia,
                                              numerocartao=row.NUMEROCARTAO,
                                              id_estrutura=self.id_structure,
                                              matricula=get_description_field(field_structure, 'MATRICULA', row),
                                              lote=lote,
                                              blanket=get_description_field(field_structure, 'BLANKET', row),
                                              dataprocessamento=datetime.today(),
                                              id_clientecorporativo=int(self.id_client),
                                              datacadastro=datetime.today().date(),
                                              datacancelamento=None,
                                              datainiciocobranca=datetime.today().date(),
                                              datafimcobranca=row.DATAFIMVIGENCIA,
                                              qtddiasendosso=None,
                                              id_statusprovisorio=None,
                                              datastatusprovisorio=None,
                                              datareativacao=None,
                                              data_lote=data_lote,
                                              data_expurgo=None,
                                              codigosusep=codigosusep,
                                              id_sucursal=id_sucursal))
        else:
            entities.append(CoveredItemEntity(id_itemcoberto=row.id_itemcoberto_y,
                                              id_plano=int(row.PLANO),
                                              id_statusitemcoberto=int(row.OPERACAO),
                                              apolice=str(row.APOLICE),
                                              datainiciovigencia=row.DATAINICIOVIGENCIA,
                                              apoliceitem=row.APOLICEITEM,
                                              datafimvigencia=datafimvigencia,
                                              numerocartao=row.NUMEROCARTAO,
                                              id_estrutura=int(self.id_structure),
                                              matricula=get_description_field(field_structure, 'MATRICULA', row),
                                              lote=lote,
                                              blanket=get_description_field(field_structure, 'BLANKET', row),
                                              dataprocessamento=datetime.today(),
                                              id_clientecorporativo=int(self.id_client),
                                              datacadastro=datetime.today().date(),
                                              datacancelamento=None,
                                              datainiciocobranca=datetime.today().date(),
                                              datafimcobranca=datafimcobranca,
                                              qtddiasendosso=None,
                                              id_statusprovisorio=None,
                                              datastatusprovisorio=None,
                                              datareativacao=None,
                                              data_lote=data_lote,
                                              data_expurgo=None,
                                              codigosusep=codigosusep,
                                              id_sucursal=id_sucursal))
        return entities
