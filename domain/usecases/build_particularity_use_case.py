# TODO- Melhorar a performance dessa class
import logging

import pandas as pd
from kink import inject

from data.usecase import ParticularityDataUseCase
from infra.entities import ParticularityEntity
from tools import check_type_num

LOGGER = logging.getLogger(__name__)


@inject
class ParticularityUseCase:
    """
        Particularity Use Case
    """

    def __init__(self, particularity_data_use_case: ParticularityDataUseCase):
        self.particularityDataUseCase = particularity_data_use_case

    def build(self, data_frame_merge, fields_structure, type_particularity, list_item_coberto) -> list:
        field_structure = fields_structure.query('tabelaDestino == "PARTICULARIDADE"')
        entities = []
        df_list_type_particularity = type_particularity
        list_particularity = self.particularityDataUseCase.find_by_list_covered_item(list_item_coberto)
        df_list_particularity = pd.DataFrame(list_particularity)

        for index, row in data_frame_merge.iterrows():
            for descricao_campo in field_structure['descricaoCampo']:
                str_descricao_campo = descricao_campo.replace(" ", "_")
                if pd.isnull(row[str_descricao_campo]):
                    continue
                valor = row[str_descricao_campo][0:ParticularityEntity.valor.type.length]
                if len(valor) == 0:
                    continue
                finder = df_list_type_particularity.query("descricao == '" + descricao_campo + "'")
                id_particularity = None
                if len(df_list_particularity) > 0:
                    particularity = df_list_particularity.query(
                        "id_itemcoberto == " + str(
                            row.id_itemcoberto) + " and id_tipoparticularidade ==" +
                        str(finder.iloc[0]['id_tipo_particularidade']))
                    id_particularity = check_type_num(
                        particularity.iloc[0]['id_particularidade'] if len(
                            particularity) > 0 else None)

                entity = ParticularityEntity(id_itemcoberto=int(row['id_itemcoberto']),
                                             id_tipoparticularidade=check_type_num(finder.iloc[0][
                                                                                       'id_tipo_particularidade']),
                                             id_particularidade=id_particularity,
                                             valor=valor)
                entities.append(entity)

        new_list = []
        for element in entities:
            if len(element.valor) == 0:
                continue
            if element is not None and not element in new_list:
                new_list.append(element)
        return new_list

    def execute_insert(self, entities):
        return self.particularityDataUseCase.insert(entities)

    def execute_update(self, entities):
        return_ = []
        LOGGER.info("INSERINDO OU ATUALIZANDO" + str(len(entities)))
        for entity in entities:
            return_.append(self.particularityDataUseCase.update([entity]))
        return return_
