import logging

from kink import inject

from data.usecase.property_covered_item_data_use_case import PropertyCoveredItemUseCase
from infra.entities import PropertyCoveredItemEntity
from tools import get_description_field, get_description_field_int, get_description_field_truncate

LOGGER = logging.getLogger(__name__)


@inject
class BuildPropertyCoveredItemUseCase:
    def __init__(self, property_covered_item_use_case: PropertyCoveredItemUseCase):
        self.property_covered_item_use_case = property_covered_item_use_case

    def build(self, data_frame_merge, fields_structure, id_client) -> list:
        entities = []
        field_structure = fields_structure.query('tabelaDestino == "PROPRITEMCOBERTO"')

        for index, row in data_frame_merge.iterrows():
            nome_proprietario = get_description_field(field_structure, 'NOMEPROPRIETARIO', row)
            entities.append(PropertyCoveredItemEntity(id_propritemcoberto=int(row['id_itemcoberto']),
                                                      nomeproprietario=nome_proprietario,
                                                      paisendpropr=get_description_field(field_structure,
                                                                                         'PAISENDPROPR', row),
                                                      nomeproprietariosoundex=get_description_field(field_structure,
                                                                                                    'NOMEPROPRIETARIOSOUNDEX',
                                                                                                    row),
                                                      cnpjcpfpropr=get_description_field(field_structure,
                                                                                         'CNPJCPFPROPR', row),
                                                      ufendpropr=get_description_field(field_structure, 'UFENDPROPR',
                                                                                       row),
                                                      cidadeendpropr=get_description_field(field_structure,
                                                                                           'CIDADEENDPROPR', row),
                                                      bairroendpropr=get_description_field(field_structure,
                                                                                           'BAIRROENDPROPR', row),
                                                      cependpropr=get_description_field(field_structure, 'CEPENDPROPR',
                                                                                        row),
                                                      logradouroendpropr=get_description_field_truncate(field_structure,
                                                                                                        'LOGRADOUROENDPROPR',
                                                                                                        row,
                                                                                                        PropertyCoveredItemEntity.logradouroendpropr.type.length),
                                                      numeroendpropr=get_description_field(field_structure,
                                                                                           'NUMEROENDPROPR', row),
                                                      complementoendpropr=get_description_field(field_structure,
                                                                                                'COMPLEMENTOENDPROPR',
                                                                                                row),
                                                      ddifonepropr=get_description_field(field_structure,
                                                                                         'DDIFONEPROPR', row),
                                                      dddfonepropr=get_description_field(field_structure,
                                                                                         'DDDFONEPROPR', row),
                                                      fonepropr=get_description_field(field_structure, 'FONEPROPR',
                                                                                      row),
                                                      id_clientecorporativo=id_client,
                                                      cnpjcpfproprpesquisa=get_description_field_int(field_structure,
                                                                                                     'CNPJCPFPROPR',
                                                                                                     row)))
        return entities

    def execute_insert(self, entities):
        return self.property_covered_item_use_case.insert(entities)

    def execute_update(self, entities):
        return_ = []
        LOGGER.info("INSERINDO OU ATUALIZADON " + str(len(entities)))
        for entity in entities:
            return_.append(self.property_covered_item_use_case.update([entity]))
        return return_
