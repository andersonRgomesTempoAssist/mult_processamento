import logging

from kink import inject

from data.usecase import PropertyCoveredDataUseCase
from infra.entities import PropertyCoveredEntity
from tools import get_description_field

LOGGER = logging.getLogger(__name__)


@inject
class PropertyCoveredUseCase:

    def __init__(self, property_covered_data_use_case: PropertyCoveredDataUseCase):
        self.property_covered_data_use_case = property_covered_data_use_case

    def build(self, data_frame_merge, fields_structure, id_client) -> list:
        entities = []
        field_structure = fields_structure.query('tabelaDestino == "IMOVELCOBERTO"')
        for index, row in data_frame_merge.iterrows():
            pais = get_description_field(field_structure, 'PAIS', row)
            uf = get_description_field(field_structure, 'UF', row)
            cidade = get_description_field(field_structure, 'CIDADE', row)
            bairro = get_description_field(field_structure, 'BAIRRO', row)
            cep = get_description_field(field_structure, 'CEP', row)
            logradouro = get_description_field(field_structure, 'LOGRADOURO', row)
            numero = get_description_field(field_structure, 'NUMERO', row)
            complemento = get_description_field(field_structure, 'COMPLEMENTO', row)
            codigoimovel = get_description_field(field_structure, 'CODIGOIMOVEL', row)
            nomeinquilino = get_description_field(field_structure, 'NOMEINQUILINO', row)
            cnpjcpfinquilino = get_description_field(field_structure, 'CNPJCPFINQUILINO', row)
            if logradouro is not None:
                logradouro = logradouro[0:PropertyCoveredEntity.logradouro.type.length]

            entities.append(
                PropertyCoveredEntity(id_imovelcoberto=int(row['id_itemcoberto']),
                                      pais=pais,
                                      uf=uf,
                                      cidade=cidade,
                                      bairro=bairro,
                                      cep=cep,
                                      logradouro=logradouro,
                                      numero=numero,
                                      complemento=complemento,
                                      codigoimovel=codigoimovel,
                                      nomeinquilino=nomeinquilino,
                                      cnpjcpfinquilino=cnpjcpfinquilino,
                                      id_clientecorporativo=id_client))
        return entities

    def execute_insert(self, entities):
        return self.property_covered_data_use_case.insert(entities)

    def execute_update(self, entities):
        return_ = []
        LOGGER.info("INSERINDO OU ATUALIZADON " + len(entities))
        for entity in entities:
            return_.append(self.property_covered_data_use_case.update([entity]))
        return return_
