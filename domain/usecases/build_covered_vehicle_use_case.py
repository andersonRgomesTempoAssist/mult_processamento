import logging

from kink import inject

from data.usecase.covered_vehicle_data_use_case import CoveredVehicleDataUseCase
from infra.entities import CoveredVehicleEntity
from tools import get_description_field, get_description_field_int
LOGGER = logging.getLogger(__name__)

@inject
class BuildCoveredVehicleUseCase:

    def __init__(self, covered_vehicle: CoveredVehicleDataUseCase):
        self.covered_vehicle = covered_vehicle

    def build(self, data_frame_merge, fields_structure, id_client) -> list:
        entities = []
        field_structure = fields_structure.query('tabelaDestino == "VEICULOCOBERTO"')
        for index, row in data_frame_merge.iterrows():
            placa = get_description_field(field_structure, 'PLACA', row)
            marca = get_description_field(field_structure, 'MARCA', row)
            modelo = get_description_field(field_structure, 'MODELO', row)
            chassi = get_description_field(field_structure, 'CHASSI', row)
            anofabric = get_description_field_int(field_structure, 'ANOFABRIC', row)
            anomodelo = get_description_field_int(field_structure, 'ANOMODELO', row)
            valormercado = get_description_field_int(field_structure, 'VALORMERCADO', row)
            cor = get_description_field(field_structure, 'COR', row)
            combustivel = get_description_field(field_structure, 'COMBUSTIVEL', row)
            flagzerokm = get_description_field(field_structure, 'FLAGZEROKM', row)
            flaggaragem = get_description_field(field_structure, 'FLAGGARAGEM', row)
            placanumerica = get_description_field(field_structure, 'PLACANUMERICA', row)
            entities.append(CoveredVehicleEntity(
                id_veiculocoberto=int(row['id_itemcoberto']),
                placa=placa,
                marca=marca,
                modelo=modelo,
                chassi=chassi,
                anofabric=anofabric,
                anomodelo=anomodelo,
                valormercado=valormercado,
                cor=cor,
                combustivel=combustivel,
                flagzerokm=flagzerokm,
                flaggaragem=flaggaragem,
                placanumerica=placanumerica,
                id_clientecorporativo=id_client))
        return entities

    def execute_insert(self, entities):
        return self.covered_vehicle.insert(entities)

    def execute_update(self, entities):
        return_ = []
        LOGGER.info("INSERINDO OU ATUALIZANDO " + str(len(entities)))
        for entity in entities:
            return_.append(self.covered_vehicle.update([entity]))
        return return_
