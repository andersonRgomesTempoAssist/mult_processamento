from kink import inject

from data.usecase import PlanoDataUseCase, ContractDataUseCase, StructureDataUseCase, TipoParticularidadeUseCase, \
    CoveredItemDataUseCase


@inject
class LoadInformationUseCase:

    def __init__(self, plano_data_use_case: PlanoDataUseCase, contract_data_use_case: ContractDataUseCase,
                 covered_item_data_use_case: CoveredItemDataUseCase, structure_data_use_case: StructureDataUseCase,
                 tipo_particularidade: TipoParticularidadeUseCase):
        self.plano_data_use_case = plano_data_use_case
        self.contract_data_use_case = contract_data_use_case
        self.covered_item_data_use_case = covered_item_data_use_case
        self.structure_data_use_case = structure_data_use_case
        self.tipo_particularidade = tipo_particularidade

    def load_information_plan_by_contract(self, id_contract):
        return self.plano_data_use_case.find_by_id_contract(id_contract)

    def load_contract_by_id(self, id_contract):
        return self.contract_data_use_case.find_by_id(id_contract)

    def load_structure_by_id(self, id_structure):
        return self.structure_data_use_case.find_by_id(id_structure)

    def load_covered_item(self, id_client, id_contract, array_policy, array_policy_item,
                          array_car_number, id_plano):
        if isinstance(array_policy, list) and isinstance(array_policy_item, list) and isinstance(array_car_number,
                                                                                                 list):
            return self.covered_item_data_use_case.find_covered_item(id_client=id_client, id_contract=id_contract,
                                                                     array_policy=array_policy,
                                                                     array_policy_item=array_policy_item,
                                                                     array_car_number=array_car_number)
        return self.covered_item_data_use_case.find_covered_item_by_keys(id_client=id_client,
                                                                         id_contract=id_contract,
                                                                         array_policy=array_policy,
                                                                         array_policy_item=array_policy_item,
                                                                         array_car_number=array_car_number, id_plano=id_plano)

    def load_type_particularity(self):
        return self.tipo_particularidade.find_all()
