from domain.usecases.validation import IValidation


class PlanValidation(IValidation):

    def __init__(self, data_frame, plans):
        self.data_frame = data_frame
        self.plans = plans

    def build_validation(self):
        if 'MESSAGE_ERROR' not in self.data_frame.columns:
            self.data_frame['MESSAGE_ERROR'] = None
        list_plan_canceled = [str(plano.id_plano) for plano in self.plans if plano.id_status == 3]
        list_plan = [str(plano.id_plano) for plano in self.plans if plano.id_status != 3]
        message = '[104] Plano cancelado no cadastro'

        index = self.data_frame['MESSAGE_ERROR'][self.data_frame['MESSAGE_ERROR'].isnull()][
            self.data_frame['PLANO'].isin(list_plan_canceled)].index.values
        if len(index) > 0:
            self.data_frame.at[index, 'MESSAGE_ERROR'] = message

        index = self.data_frame['MESSAGE_ERROR'][self.data_frame['MESSAGE_ERROR'].isnull()][
            ~self.data_frame['PLANO'].isin(list_plan)].index.values
        if len(index) > 0:
            message = '[002] Plano não cadastrado para estrutura'
            self.data_frame.at[index, 'MESSAGE_ERROR'] = message

        return self.data_frame
