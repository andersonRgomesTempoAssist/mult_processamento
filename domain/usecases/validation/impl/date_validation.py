import pandas as pd
import datetime

from domain.hepers import ExceptionDomain, ExceptionDataFrameEmpty
from domain.usecases.validation import IValidation


def update_data_frame(data_frame_register, error_message, index):
    if len(index) > 0:
        for index_ in index:
            data_frame_register.at[index_, 'MESSAGE_ERROR'] = error_message
    return data_frame_register


class DateValidate(IValidation):

    def __init__(self, data_frame_register, contract, format_str: str):
        self.data_frame_register = data_frame_register
        self.contract = contract
        self.format_str = format_str

    def build_validation(self):
        if 'MESSAGE_ERROR' not in self.data_frame_register.columns:
            self.data_frame_register['MESSAGE_ERROR'] = None

        if 'MESSAGE_WARN' not in self.data_frame_register.columns:
            self.data_frame_register['MESSAGE_WARN'] = None

        if self.data_frame_register['MESSAGE_ERROR'].dtypes == 'float64':
            self.data_frame_register["MESSAGE_ERROR"] = self.data_frame_register["MESSAGE_ERROR"].astype(
                {'MESSAGE_ERROR': str})
            self.data_frame_register['MESSAGE_ERROR'] = None

        self.data_frame_register['DATAINICIOVIGENCIA'] = pd.to_datetime(
            self.data_frame_register['DATAINICIOVIGENCIA'].str.replace('/', ''),
            format=self.format_str, errors='coerce').dt.date

        self.data_frame_register['DATAFIMVIGENCIA'] = pd.to_datetime(
            self.data_frame_register['DATAFIMVIGENCIA'].str.replace('/', ''),
            format=self.format_str, errors='coerce').dt.date

        index = self.data_frame_register.query("DATAFIMVIGENCIA.isna() == True").index.values
        if len(index) > 0:
            for index_ in index:
                self.data_frame_register.at[index_, 'DATAFIMVIGENCIA'] = pd.Timestamp.max.date()

        error_message = '[005] Data fim enviada é menor que a Data inicio enviada'
        index = self.data_frame_register.query("MESSAGE_ERROR.isnull()")[
            self.data_frame_register['DATAINICIOVIGENCIA'] >= self.data_frame_register['DATAFIMVIGENCIA']].index.values

        if len(index) > 0:
            for index_ in index:
                self.data_frame_register.at[index_, 'MESSAGE_ERROR'] = error_message

        error_message = '[016] Data de inicio de vigência enviada é maior que data de processamento'
        index = self.data_frame_register[self.data_frame_register['MESSAGE_ERROR'].isnull()][
            self.data_frame_register['DATAINICIOVIGENCIA'] > datetime.date.today()].index.values

        if len(index) > 0:
            for index_ in index:
                self.data_frame_register.at[index_, 'MESSAGE_ERROR'] = error_message

        warn_message = '[034] Item adicionado com data futura'
        index = self.data_frame_register[self.data_frame_register['MESSAGE_ERROR'].isnull()][
            self.data_frame_register['DATAINICIOVIGENCIA'] == datetime.date.today()].index.values

        if len(index) > 0:
            for index_ in index:
                self.data_frame_register.at[index_, 'MESSAGE_WARN'] = warn_message

        error_message = '[014] Data Fim de Vigencia informada é menor que a Data de Inicio na USS'

        index = self.data_frame_register.query("MESSAGE_ERROR.isnull()")[
            self.contract.datainicio > self.data_frame_register['DATAFIMVIGENCIA']
            ].index.values

        if len(index) > 0:
            for index_ in index:
                self.data_frame_register.at[index_, 'MESSAGE_ERROR'] = error_message

        warn_message = '[007] Data de Inicio informada é menor que a Data de Inicio existente no registro na USS'

        index = self.data_frame_register.query("MESSAGE_ERROR.isnull()")[
            self.contract.datainicio > self.data_frame_register['DATAINICIOVIGENCIA']
            ].index.values

        if len(index) > 0:
            for index_ in index:
                self.data_frame_register.at[index_, 'MESSAGE_WARN'] = warn_message

        error_message = '[033] Data Fim Vigencia informada é menor ou igual a data de processamento | VENCIDO '
        index = self.data_frame_register.query("MESSAGE_ERROR.isnull()")[
            self.data_frame_register['DATAFIMVIGENCIA'] <= datetime.date.today()
            ].index.values

        if len(index) > 0:
            for index_ in index:
                self.data_frame_register.at[index_, 'MESSAGE_ERROR'] = error_message

        return self.data_frame_register
