import pandas

from domain.usecases.validation import IValidation


class ExecuteValidation:
    validation_implements = None

    def __init__(self, data_frame, structure):
        self.data_frame = data_frame
        self.structure = structure

    def __setValidation__(self, validation: IValidation):
        self.validation_implements = validation

    def validation(self) -> pandas.DataFrame:
        data_frame_validate = self.validation_implements.build_validation()
        return data_frame_validate
