import logging

from kink import inject

from data.usecase import CoveredLivesDataUseCase
from infra.entities import CoveredLifeEntity
from tools import get_description_field, get_description_field_date

LOGGER = logging.getLogger(__name__)


@inject()
class BuildCoveredLifeUseCase:
    def __init__(self, date_format: str, covered_lives_data_use_case: CoveredLivesDataUseCase):
        self.covered_lives_data_use_case = covered_lives_data_use_case
        self.date_format = date_format

    def build(self, data_frame_merge, fields_structure, id_client) -> list:
        entities = []
        field_structure = fields_structure.query('tabelaDestino == "VIDACOBERTA"')

        for index, row in data_frame_merge.iterrows():
            sexo = get_description_field(field_structure, 'SEXO', row)
            nome = get_description_field(field_structure, 'NOME', row)
            nome_soundex = get_description_field(field_structure, 'NOMESOUNDEX', row)
            profissao = get_description_field(field_structure, 'PROFISSAO', row)
            cpf = get_description_field(field_structure, 'CPF', row)
            rg = get_description_field(field_structure, 'RG', row)
            estado_civil = get_description_field(field_structure, 'ESTADOCIVIL', row)
            data_nascimento = get_description_field_date(field_structure, 'DATANASCIMENTO', row, self.date_format)
            cpf_pesquisa = get_description_field(field_structure, 'CPFPESQUISA', row)
            entities.append(
                CoveredLifeEntity(
                    id_vidacoberta=int(row['id_itemcoberto']),
                    sexo=sexo,
                    nome=nome,
                    nomesoundex=nome_soundex,
                    profissao=profissao,
                    cpf=cpf,
                    rg=rg,
                    datanascimento=data_nascimento,
                    estadocivil=estado_civil,
                    cpfpesquisa=None if cpf_pesquisa is None else int(cpf_pesquisa),
                    id_clientecorporativo=id_client)
            )
        return entities

    def execute_insert(self, entities):
        return self.covered_lives_data_use_case.insert(entities)

    def execute_update(self, entities):
        return_ = []
        LOGGER.info("INSERINDO OU ATUALIZANDO" + str(len(entities)))
        for entity in entities:
            return_.append(self.covered_lives_data_use_case.update([entity]))
        return return_
