import logging
from collections import namedtuple

import sqlalchemy
from sqlalchemy.orm.exc import NoResultFound
from infra.helpers import ExceptionRepository

from infra.config import DBConnectionHandler
from infra.entities import CoveredItemEntity

LOGGER = logging.getLogger(__name__)


class CoveredItemRepository:

    def find_covered_item_by_keys(self, id_client, id_contract, array_policy, array_policy_item,
                                  array_car_number, id_plano):
        for i in DBConnectionHandler.__session__():
            try:
                str_query = "SELECT " \
                            "I.ID_ITEMCOBERTO, " \
                            "I.ID_PLANO, " \
                            "I.ID_STATUSITEMCOBERTO, " \
                            "I.APOLICE, " \
                            "I.DATAINICIOVIGENCIA, " \
                            "I.APOLICEITEM, " \
                            "I.DATAFIMVIGENCIA, " \
                            "I.NUMEROCARTAO, " \
                            "I.DATAPROCESSAMENTO, " \
                            "I.DATACADASTRO, " \
                            "I.DATACANCELAMENTO, " \
                            "I.DATAINICIOCOBRANCA, " \
                            "I.DATAFIMCOBRANCA, " \
                            "I.QTDDIASENDOSSO, " \
                            "I.ID_STATUSPROVISORIO, " \
                            "I.DATASTATUSPROVISORIO, " \
                            "I.ID_CLIENTECORPORATIVO, " \
                            "I.DATAREATIVACAO, " \
                            "I.LOTE, " \
                            "I.DATA_LOTE, " \
                            "I.ID_ESTRUTURA, " \
                            "I.DATA_EXPURGO, " \
                            "I.MATRICULA, " \
                            "I.CODIGOSUSEP, " \
                            "I.BLANKET, " \
                            "I.ID_SUCURSAL, " \
                            "P.CNPJCPFPROPR " \
                            "FROM ITEMCOBERTO I " \
                            "INNER JOIN PROPRITEMCOBERTO P ON(I.ID_ITEMCOBERTO = P.ID_PROPRITEMCOBERTO) " \
                            "INNER JOIN PLANO PL ON(I.ID_PLANO = PL.ID_PLANO) " \
                            "INNER JOIN contrato c ON(PL.ID_CONTRATO = C.id_contrato) " \
                            "WHERE  i.Id_Clientecorporativo = " + str(id_client) + \
                            " AND I.ID_PLANO = " + str(id_plano) + \
                            " AND pl.id_contrato = " + str(id_contract) + \
                            " AND i.APOLICE = '" + array_policy + "'" + \
                            " AND i.APOLICEITEM = '" + array_policy_item + "'" + \
                            " AND i.NUMEROCARTAO  = '" + array_car_number + "'"
                results = i.execute(str_query)
                Record = namedtuple("Record", results.keys())
                records = [Record(*r) for r in results.fetchall()]
                return records
            except sqlalchemy.exc.OperationalError as error:
                raise ExceptionRepository('000', 'Internal server Error')
                i.throw(error)

            except NoResultFound:
                raise ExceptionRepository('000', 'Not found Plans')

            except Exception as error:
                raise ExceptionRepository('000', 'Internal server Error')
                i.throw(error)

    def find_covered_item(self, id_client, id_contract, array_policy, array_policy_item,
                          array_car_number):
        for i in DBConnectionHandler.__session__():
            try:
                str_query = "SELECT " \
                            "I.ID_ITEMCOBERTO, " \
                            "I.ID_PLANO, " \
                            "I.ID_STATUSITEMCOBERTO, " \
                            "I.APOLICE, " \
                            "I.DATAINICIOVIGENCIA, " \
                            "I.APOLICEITEM, " \
                            "I.DATAFIMVIGENCIA, " \
                            "I.NUMEROCARTAO, " \
                            "I.DATAPROCESSAMENTO, " \
                            "I.DATACADASTRO, " \
                            "I.DATACANCELAMENTO, " \
                            "I.DATAINICIOCOBRANCA, " \
                            "I.DATAFIMCOBRANCA, " \
                            "I.QTDDIASENDOSSO, " \
                            "I.ID_STATUSPROVISORIO, " \
                            "I.DATASTATUSPROVISORIO, " \
                            "I.ID_CLIENTECORPORATIVO, " \
                            "I.DATAREATIVACAO, " \
                            "I.LOTE, " \
                            "I.DATA_LOTE, " \
                            "I.ID_ESTRUTURA, " \
                            "I.DATA_EXPURGO, " \
                            "I.MATRICULA, " \
                            "I.CODIGOSUSEP, " \
                            "I.BLANKET, " \
                            "I.ID_SUCURSAL, " \
                            "P.CNPJCPFPROPR " \
                            "FROM ITEMCOBERTO I " \
                            "INNER JOIN PROPRITEMCOBERTO P ON(I.ID_ITEMCOBERTO = P.ID_PROPRITEMCOBERTO) " \
                            "INNER JOIN PLANO PL ON(I.ID_PLANO = PL.ID_PLANO) " \
                            "INNER JOIN contrato c ON(PL.ID_CONTRATO = C.id_contrato) " \
                            "WHERE  i.Id_Clientecorporativo = " + str(id_client) + \
                            " AND pl.id_contrato = " + str(id_contract)

                if len(array_policy) > 1000:
                    filter_select = ""
                    n = len(array_policy) // 1000 + 1
                    splinted = [array_policy[i::n] for i in range(n)]
                    for sp in splinted:
                        filter_select += " OR i.APOLICE IN (" + "'" + "','".join(sp) + "'" + ")"
                    str_query += " AND (" + filter_select[3:] + ")"
                elif len(array_policy) > 0:
                    str_query += " AND i.APOLICE IN (" + "'" + "','".join(array_policy) + "'" + ")"
                str_query += " AND i.APOLICEITEM IN (" + "'" + "','".join(array_policy_item) + "'" + ")"
                if len(array_car_number) > 1000:
                    filter_select = ""
                    n = len(array_car_number) // 1000 + 1
                    splinted = [array_car_number[i::n] for i in range(n)]
                    for sp in splinted:
                        filter_select += " OR i.NUMEROCARTAO IN (" + "'" + "','".join(sp) + "'" + ")"
                    str_query += " AND (" + filter_select[3:] + ")"
                elif len(array_car_number) > 0:
                    str_query += " AND i.NUMEROCARTAO  IN (" + "'" + "','".join(array_car_number) + "'" + ")"
                results = i.execute(str_query)
                Record = namedtuple("Record", results.keys())
                records = [Record(*r) for r in results.fetchall()]
                return records
            except sqlalchemy.exc.OperationalError as error:
                i.throw(error)
                raise ExceptionRepository('000', 'Internal server Error')
            except NoResultFound:
                raise ExceptionRepository('000', 'Not found Plans')

            except Exception as error:
                i.throw(error)
                raise ExceptionRepository('000', 'Internal server Error')

    def insert_covered_item(self, entities):
        for i in DBConnectionHandler.__session__():
            try:
                i.bulk_save_objects(entities, return_defaults=True)
                i.commit()
                return entities
            except sqlalchemy.exc.OperationalError as error:
                LOGGER.error(error)
                raise ExceptionRepository('000', 'Internal server Error')
                self.db_connection.throw(error)
            except Exception as error:
                LOGGER.error(erro)
                self.db_connection.throw(error)
                raise ExceptionRepository('000', 'Internal server Error')

    def update_covered_item(self, entities):
        mappings = []
        entities_insert = []
        entities_insert_result = None
        for entity in entities:
            if entity.id_itemcoberto is None:
                entities_insert.append(entity)
                entities.remove(entity)
        if len(entities_insert):
            entities_insert_result = self.insert_covered_item(entities_insert)

        for entity in entities:
            if entity.id_itemcoberto is not None:
                mapper = {'id_itemcoberto': entity.id_itemcoberto,
                          'id_plano': entity.id_plano,
                          'id_statusitemcoberto': entity.id_statusitemcoberto,
                          'apolice': entity.apolice,
                          'datainiciovigencia': entity.datainiciovigencia,
                          'apoliceitem': entity.apoliceitem,
                          'datafimvigencia': entity.datafimvigencia,
                          'numerocartao': entity.numerocartao,
                          'dataprocessamento': entity.dataprocessamento,
                          'datacadastro': entity.datacadastro,
                          'datacancelamento': entity.datacancelamento,
                          'datainiciocobranca': entity.datainiciocobranca,
                          'datafimcobranca': entity.datafimcobranca,
                          'qtddiasendosso': entity.qtddiasendosso,
                          'id_statusprovisorio': entity.id_statusprovisorio,
                          'datastatusprovisorio': entity.datastatusprovisorio,
                          'id_clientecorporativo': entity.id_clientecorporativo,
                          'datareativacao': entity.datareativacao,
                          'lote': entity.lote,
                          'data_lote': entity.data_lote,
                          'id_estrutura': entity.id_estrutura,
                          'data_expurgo': entity.data_expurgo,
                          'matricula': None if entity.matricula == 'nan' else entity.matricula,
                          'codigosusep': entity.codigosusep,
                          'blanket': entity.blanket,
                          'id_sucursal': entity.id_sucursal}
                mappings.append(mapper)
        for i in DBConnectionHandler.__session__():
            try:
                i.bulk_update_mappings(CoveredItemEntity, mappings)
                i.commit()
                if entities_insert_result is not None:
                    entities.extend(entities_insert_result)
                return entities
            except sqlalchemy.exc.OperationalError as error:
                raise ExceptionRepository('000', error)
            except Exception as error:
                raise ExceptionRepository('000', error)
