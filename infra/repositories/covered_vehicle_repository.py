import sqlalchemy
from collections import namedtuple

from infra.helpers import ExceptionRepository
from infra.config import DBConnectionHandler
from infra.entities import CoveredVehicleEntity


class CoveredVehicleRepository:

    def insert(self, entities):
        for i in DBConnectionHandler.__session__():
            try:
                i.bulk_save_objects(entities, return_defaults=True)
                i.commit()
                return entities
            except sqlalchemy.exc.OperationalError as error:
                raise ExceptionRepository('000', 'Internal server Error')
                self.db_connection.throw(error)
            except Exception as error:
                self.db_connection.throw(error)
                raise ExceptionRepository('000', 'Internal server Error')

    def find_list_id_covered_item(self, covered_item: []):
        for i in DBConnectionHandler.__session__():
            try:
                Record = namedtuple("Record", [
                    'id_veiculocoberto',
                    'marca',
                    'modelo',
                    'anofabric',
                    'anomodelo',
                    'placa',
                    'chassi',
                    'cor',
                    'combustivel',
                    'flagzerokm',
                    'valormercado',
                    'flaggaragem',
                    'placanumerica',
                    'id_clientecorporativo'
                ])
                results = None
                if len(covered_item) > 1000:
                    filter_select = ""
                    str_query = "select id_veiculocoberto, marca, modelo, anofabric, anomodelo, placa, chassi, cor, " \
                                "combustivel, flagzerokm, valormercado, flaggaragem, placanumerica, " \
                                "id_clientecorporativo FROM VEICULOCOBERTO " \
                                "where 1=1"

                    n = len(covered_item) // 1000 + 1
                    splinted = [covered_item[i::n] for i in range(n)]
                    for sp in splinted:
                        filter_select += " OR id_veiculocoberto IN (" + str(sp).replace('[', '').replace(']',
                                                                                                         '') + ")"
                    str_query += " AND (" + filter_select[3:] + ")"
                    results = i.execute(str_query)
                    Record = namedtuple("Record", results.keys())
                    records = [Record(*r) for r in results.fetchall()]
                    return records
                else:
                    results = i.query(CoveredVehicleEntity).filter(
                        CoveredVehicleEntity.id_veiculocoberto.in_(covered_item)).all()
                    return [
                        Record(
                            id_veiculocoberto=result.id_veiculocoberto,
                            marca=result.marca,
                            modelo=result.modelo,
                            anofabric=result.anofabric,
                            anomodelo=result.anomodelo,
                            placa=result.placa,
                            chassi=result.chassi,
                            cor=result.cor,
                            combustivel=result.combustivel,
                            flagzerokm=result.flagzerokm,
                            valormercado=result.valormercado,
                            flaggaragem=result.flaggaragem,
                            placanumerica=result.placanumerica,
                            id_clientecorporativo=result.id_clientecorporativo
                        )
                        for result in results
                    ]
            except sqlalchemy.exc.OperationalError as error:
                raise ExceptionRepository('000', 'Internal server Error')
                self.db_connection.throw(error)
            except Exception as error:
                self.db_connection.throw(error)
                raise ExceptionRepository('000', 'Internal server Error')

    def update(self, entities):
        mappings = []
        mappings_insert = []
        id_veiculocoberto = [entity.id_veiculocoberto for entity in entities]
        list_properties = self.find_list_id_covered_item(id_veiculocoberto)
        if list_properties is not None and len(list_properties) > 0:
            for entity in entities:
                exists = list(filter(lambda x: entity.id_veiculocoberto in x, list_properties))
                if len(exists) > 0:
                    mapper = {'id_veiculocoberto': entity.id_veiculocoberto,
                              'marca': entity.marca,
                              'modelo': entity.modelo,
                              'anofabric': entity.anofabric,
                              'anomodelo': entity.anomodelo,
                              'placa': entity.placa,
                              'chassi': entity.chassi,
                              'cor': entity.cor,
                              'combustivel': entity.combustivel,
                              'flagzerokm': entity.flagzerokm,
                              'valormercado': entity.valormercado,
                              'flaggaragem': entity.flaggaragem,
                              'placanumerica': entity.placanumerica,
                              'id_clientecorporativo': entity.id_clientecorporativo
                              }
                    mappings.append(mapper)
                else:
                    mappings_insert.append(entity)
            self.insert(mappings_insert)
        else:
            return self.insert(entities)
        if len(mappings) > 0:
            for i in DBConnectionHandler.__session__():
                try:
                    i.bulk_update_mappings(CoveredVehicleEntity, mappings)
                    i.commit()
                    return entities
                except sqlalchemy.exc.OperationalError as error:
                    raise ExceptionRepository('000', 'Internal server Error')
                    self.db_connection.throw(error)
                except Exception as error:
                    self.db_connection.throw(error)
                    raise ExceptionRepository('000', 'Internal server Error')
