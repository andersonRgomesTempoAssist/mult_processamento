import sqlalchemy
from collections import namedtuple

from sqlalchemy.orm.exc import NoResultFound
from infra.entities.property_covered_item_entity import PropertyCoveredItemEntity
from infra.helpers import ExceptionRepository
from infra.config import DBConnectionHandler


class PropertyCoveredItemRepository:

    def find_by_id(self, id_property):
        for i in DBConnectionHandler.__session__():
            try:
                result = i.query(PropertyCoveredItemEntity).filter(
                    PropertyCoveredItemEntity.id_propritemcoberto == id_property).one()
                return result
            except sqlalchemy.exc.OperationalError as error:
                raise ExceptionRepository('000', 'Internal server Error')
                i.throw(error)
            except NoResultFound:
                raise ExceptionRepository('000', 'Not found Plans')
            except Exception as error:
                raise ExceptionRepository('000', 'Internal server Error')
                i.throw(error)

    def find_list_id_covered_item(self, covered_item: []):
        for i in DBConnectionHandler.__session__():
            try:
                Record = namedtuple("Record", [
                    "id_propritemcoberto",
                    "nomeproprietario",
                    "paisendpropr",
                    "nomeproprietariosoundex",
                    "cnpjcpfpropr",
                    "ufendpropr",
                    "cidadeendpropr",
                    "bairroendpropr",
                    "cependpropr",
                    "logradouroendpropr",
                    "numeroendpropr",
                    "complementoendpropr",
                    "ddifonepropr",
                    "dddfonepropr",
                    "fonepropr",
                    "id_clientecorporativo",
                    "cnpjcpfproprpesquisa",
                ])
                results = None
                if len(covered_item) > 1000:
                    filter_select = ""
                    str_query = "select id_propritemcoberto,nomeproprietario, paisendpropr, nomeproprietariosoundex, " \
                                "cnpjcpfpropr, ufendpropr, cidadeendpropr, bairroendpropr, cependpropr, " \
                                "logradouroendpropr, numeroendpropr, complementoendpropr, ddifonepropr, dddfonepropr, " \
                                "fonepropr, id_clientecorporativo, cnpjcpfproprpesquisa FROM PROPRITEMCOBERTO " \
                                "where 1=1"

                    n = len(covered_item) // 1000 + 1
                    splinted = [covered_item[i::n] for i in range(n)]
                    for sp in splinted:
                        filter_select += " OR id_propritemcoberto IN (" + str(sp).replace('[', '').replace(']',
                                                                                                           '') + ")"
                    str_query += " AND (" + filter_select[3:] + ")"
                    results = i.execute(str_query)
                    Record = namedtuple("Record", results.keys())
                    records = [Record(*r) for r in results.fetchall()]
                    return records
                else:
                    results = i.query(PropertyCoveredItemEntity).filter(
                        PropertyCoveredItemEntity.id_propritemcoberto.in_(covered_item)).all()
                    return [

                        Record(
                            id_propritemcoberto=result.id_propritemcoberto,
                            nomeproprietario=result.nomeproprietario,
                            paisendpropr=result.paisendpropr,
                            nomeproprietariosoundex=result.nomeproprietariosoundex,
                            cnpjcpfpropr=result.cnpjcpfpropr,
                            ufendpropr=result.ufendpropr,
                            cidadeendpropr=result.cidadeendpropr,
                            bairroendpropr=result.bairroendpropr,
                            cependpropr=result.cependpropr,
                            logradouroendpropr=result.logradouroendpropr,
                            numeroendpropr=result.numeroendpropr,
                            complementoendpropr=result.complementoendpropr,
                            ddifonepropr=result.ddifonepropr,
                            dddfonepropr=result.dddfonepropr,
                            fonepropr=result.fonepropr,
                            id_clientecorporativo=result.id_clientecorporativo,
                            cnpjcpfproprpesquisa=result.cnpjcpfproprpesquisa

                        )
                        for result in results
                    ]
            except sqlalchemy.exc.OperationalError as error:
                raise ExceptionRepository('000', 'Internal server Error')
                self.db_connection.throw(error)
            except Exception as error:
                self.db_connection.throw(error)
                raise ExceptionRepository('000', 'Internal server Error')


    def insert(self, entities):
        for i in DBConnectionHandler.__session__():
            try:
                i.bulk_save_objects(entities, return_defaults=True)
                i.commit()
                return entities
            except sqlalchemy.exc.OperationalError as error:
                raise ExceptionRepository('000', 'Internal server Error')
                self.db_connection.throw(error)
            except Exception as error:
                self.db_connection.throw(error)
                raise ExceptionRepository('000', 'Internal server Error')

    def update(self, entities):
        mappings = []
        mappings_insert = []
        id_propritemcoberto = [entity.id_propritemcoberto for entity in entities]
        list_properties = self.find_list_id_covered_item(id_propritemcoberto)
        if list_properties is not None and len(list_properties) > 0:
            for entity in entities:
                exists = list(filter(lambda x: entity.id_propritemcoberto in x, list_properties))
                if len(exists) > 0:
                    mapper = {'id_propritemcoberto': entity.id_propritemcoberto,
                              'nomeproprietario': entity.nomeproprietario,
                              'paisendpropr': entity.paisendpropr,
                              'nomeproprietariosoundex': entity.nomeproprietariosoundex,
                              'cnpjcpfpropr': entity.cnpjcpfpropr,
                              'ufendpropr': entity.ufendpropr,
                              'cidadeendpropr': entity.cidadeendpropr,
                              'bairroendpropr': entity.bairroendpropr,
                              'cependpropr': entity.cependpropr,
                              'logradouroendpropr': entity.logradouroendpropr,
                              'numeroendpropr': entity.numeroendpropr,
                              'complementoendpropr': entity.complementoendpropr,
                              'ddifonepropr': entity.ddifonepropr,
                              'dddfonepropr': entity.dddfonepropr,
                              'fonepropr': entity.fonepropr,
                              'id_clientecorporativo': entity.id_clientecorporativo,
                              'cnpjcpfproprpesquisa': entity.cnpjcpfproprpesquisa,
                              }
                    mappings.append(mapper)
                else:
                    mappings_insert.append(entity)
        else:
            return self.insert(entities)

        if len(mappings_insert) > 0:
            self.insert(mappings_insert)

        if len(mappings) > 0:
            for i in DBConnectionHandler.__session__():
                try:
                    i.bulk_update_mappings(PropertyCoveredItemEntity, mappings)
                    i.commit()
                    return entities
                except sqlalchemy.exc.OperationalError as error:
                    raise ExceptionRepository('000', 'Internal server Error')
                    self.db_connection.throw(error)
                except Exception as error:
                    self.db_connection.throw(error)
                    raise ExceptionRepository('000', 'Internal server Error')
