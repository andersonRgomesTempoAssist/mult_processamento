import cx_Oracle
import sqlalchemy

from collections import namedtuple
from infra.helpers import ExceptionRepository
from infra.config import DBConnectionHandler
from infra.entities import ParticularityEntity


class ParticularityRepository:

    def find_list_id_covered_item(self, covered_item: []):
        for i in DBConnectionHandler.__session__():
            try:
                Record = namedtuple("Record", [
                    "id_particularidade",
                    "id_itemcoberto",
                    "id_tipoparticularidade",
                    "valor"
                ])
                results = None
                if len(covered_item) > 1000:
                    filter_select = ""
                    str_query = "SELECT id_particularidade, id_itemcoberto, id_tipoparticularidade, VALOR " \
                                "FROM PARTICULARIDADE WHERE 1=1 "

                    n = len(covered_item) // 1000 + 1
                    splinted = [covered_item[i::n] for i in range(n)]
                    for sp in splinted:
                        filter_select += " OR ID_ITEMCOBERTO IN (" + str(sp).replace('[', '').replace(']', '') + ")"
                    str_query += " AND (" + filter_select[3:] + ")"
                    results = i.execute(str_query)
                    Record = namedtuple("Record", results.keys())
                    records = [Record(*r) for r in results.fetchall()]
                    return records
                else:
                    results = i.query(ParticularityEntity).filter(
                        ParticularityEntity.id_itemcoberto.in_(covered_item)).all()
                    return [
                        Record(
                            id_particularidade=result.id_particularidade,
                            id_itemcoberto=result.id_itemcoberto,
                            id_tipoparticularidade=result.id_tipoparticularidade,
                            valor=result.valor,
                        )
                        for result in results
                    ]
            except sqlalchemy.exc.OperationalError as error:
                raise ExceptionRepository('000', 'Internal server Error')
                i.throw(error)
            except Exception as error:
                self.db_connection.throw(error)
                raise ExceptionRepository('000', 'Internal server Error')

    def insert(self, entities):
        with DBConnectionHandler() as db_connection:
            try:
                db_connection.session.bulk_save_objects(entities, return_defaults=True)
                db_connection.session.commit()
                return entities
            except sqlalchemy.exc.OperationalError as error:
                db_connection.session.rollback()
                print(error)
                raise ExceptionRepository('000', 'Internal server Error')
            except cx_Oracle.IntegrityError as error:
                print(error)
            except Exception as error:
                db_connection.session.rollback()
                print(error)
                raise ExceptionRepository('000', error)
            finally:
                db_connection.session.close()

    def update(self, entities):
        mappings = []
        mappings_insert = []

        for entity in entities:
            if entity is not None and entity.id_particularidade is not None:
                mapper = {'id_particularidade': entity.id_particularidade,
                          'id_itemcoberto': entity.id_itemcoberto,
                          'id_tipoparticularidade': entity.id_tipoparticularidade,
                          'valor': entity.valor
                          }
                mappings.append(mapper)
            elif entity is not None:
                mappings_insert.append(entity)

        if len(mappings_insert) > 0:
            self.insert(mappings_insert)

        for i in DBConnectionHandler.__session__():
            try:
                i.bulk_update_mappings(ParticularityEntity, mappings)
                i.commit()
                return entities
            except sqlalchemy.exc.OperationalError as error:
                raise ExceptionRepository('000', 'Internal server Error')
                self.db_connection.throw(error)
            except Exception as error:
                self.db_connection.throw(error)
                raise ExceptionRepository('000', 'Internal server Error')
