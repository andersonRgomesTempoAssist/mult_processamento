import sqlalchemy
from collections import namedtuple
from infra.entities.property_covered_entity import PropertyCoveredEntity
from infra.helpers import ExceptionRepository
from infra.config import DBConnectionHandler


class PropertyCoveredRepository:

    def find_list_id_covered_item(self, covered_item: []):
        for i in DBConnectionHandler.__session__():
            try:
                Record = namedtuple("Record", [
                    "id_imovelcoberto",
                    "pais",
                    "uf",
                    "cidade",
                    "bairro",
                    "cep",
                    "logradouro",
                    "numero",
                    "complemento",
                    "id_clientecorporativo",
                    "codigoimovel",
                    "nomeinquilino",
                    "cnpjcpfinquilino",
                ])
                results = None
                if len(covered_item) > 1000:
                    filter_select = ""
                    str_query = "select ID_IMOVELCOBERTO, PAIS,UF,CIDADE,BAIRRO,CEP,LOGRADOURO," \
                                "NUMERO,COMPLEMENTO,ID_CLIENTECORPORATIVO,CODIGOIMOVEL,NOMEINQUILINO," \
                                "CNPJCPFINQUILINO, FROM IMOVELCOBERTO " \
                                "where 1=1"
                    n = len(covered_item) // 1000 + 1
                    splinted = [covered_item[i::n] for i in range(n)]
                    for sp in splinted:
                        filter_select += " OR ID_IMOVELCOBERTO IN (" + str(sp).replace('[', '').replace(']',
                                                                                                         '') + ")"
                    str_query += " AND (" + filter_select[3:] + ")"
                    results = i.execute(str_query)
                    Record = namedtuple("Record", results.keys())
                    records = [Record(*r) for r in results.fetchall()]
                    return records
                else:
                    results = i.query(PropertyCoveredEntity).filter(
                        PropertyCoveredEntity.id_imovelcoberto.in_(covered_item)).all()
                    return [
                        Record(
                            id_imovelcoberto=result.id_imovelcoberto,
                            pais=result.pais,
                            uf=result.uf,
                            cidade=result.cidade,
                            bairro=result.bairro,
                            cep=result.cep,
                            logradouro=result.logradouro,
                            numero=result.numero,
                            complemento=result.complemento,
                            id_clientecorporativo=result.id_clientecorporativo,
                            codigoimovel=result.codigoimovel,
                            nomeinquilino=result.nomeinquilino,
                            cnpjcpfinquilino=result.cnpjcpfinquilino,
                        )
                        for result in results
                    ]
            except sqlalchemy.exc.OperationalError as error:
                raise ExceptionRepository('000', 'Internal server Error')
                self.db_connection.throw(error)
            except Exception as error:
                self.db_connection.throw(error)
                raise ExceptionRepository('000', 'Internal server Error')

    def insert(self, entities):
        for i in DBConnectionHandler.__session__():
            try:
                i.bulk_save_objects(entities, return_defaults=True)
                i.commit()
                return entities
            except sqlalchemy.exc.OperationalError as error:
                raise ExceptionRepository('000', 'Internal server Error')
                self.db_connection.throw(error)
            except Exception as error:
                self.db_connection.throw(error)
                raise ExceptionRepository('000', 'Internal server Error')

    def update(self, entities):
        mappings = []
        mappings_insert = []
        id_imovelcoberto = [entity.id_imovelcoberto for entity in entities]
        list_properties = self.find_list_id_covered_item(id_imovelcoberto)
        if list_properties is not None and len(list_properties) > 0:
            for entity in entities:
                exists = list(filter(lambda x: entity.id_imovelcoberto in x, list_properties))
                if len(exists) > 0:
                    mapper = {'id_imovelcoberto': entity.id_imovelcoberto,
                              'pais': entity.pais,
                              'uf': entity.uf,
                              'cidade': entity.cidade,
                              'bairro': entity.bairro,
                              'cep': entity.cep,
                              'logradouro': entity.logradouro,
                              'numero': entity.numero,
                              'complemento': entity.complemento,
                              'id_clientecorporativo': entity.id_clientecorporativo,
                              'codigoimovel': entity.codigoimovel,
                              'nomeinquilino': entity.nomeinquilino,
                              'cnpjcpfinquilino': entity.cnpjcpfinquilino
                              }
                    mappings.append(mapper)
                else:
                    mappings_insert.append(entity)
            if len(mappings_insert) > 0:
                self.insert(mappings_insert)
        else:
            return self.insert(entities)
        if len(mappings) > 0:
            for i in DBConnectionHandler.__session__():
                try:
                    i.bulk_update_mappings(PropertyCoveredEntity, mappings)
                    i.commit()
                    return entities
                except sqlalchemy.exc.OperationalError as error:
                    raise ExceptionRepository('000', 'Internal server Error')
                    self.db_connection.throw(error)
                except Exception as error:
                    self.db_connection.throw(error)
                    raise ExceptionRepository('000', 'Internal server Error')
