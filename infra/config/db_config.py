import logging
import logging.config
import string
from sys import platform

import cx_Oracle
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from kink import di

from tools import get_logger_config

logging.config.fileConfig(fname=get_logger_config(), disable_existing_loggers=False,
                          defaults={"logfilename": "mult_process.log"})
LOGGER = logging.getLogger(__name__)


class DBConnectionHandler:
    """Sqlalchemy database connection"""

    def __init__(self):
        self.__connection_string = di["string_connection"]
        self.session = None
        try:
            LOGGER.debug("ADD Folder oracle client {}".format(di["lib_dir"]))
            if cx_Oracle.clientversion() is None:
                cx_Oracle.init_oracle_client(lib_dir=di["lib_dir"])
        except Exception as error:
            if platform == "win32":
                cx_Oracle.init_oracle_client(lib_dir=di["lib_dir"])
                return
            LOGGER.error(string.Formatter("Unexpected error, %s", error))
            raise

    def get_engine(self):
        """Return connection Engine
        :param - None
        :return - engine connection to Database
        """
        engine = create_engine(self.__connection_string, coerce_to_unicode=True)
        return engine

    def __enter__(self):
        engine = create_engine(self.__connection_string)
        session_maker = sessionmaker()
        self.session = session_maker(bind=engine)
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.session.close()  # pylint: disable=no-member

    @staticmethod
    def __session__():
        """Static Method"""
        with DBConnectionHandler() as db_connection:
            try:
                yield db_connection.session
            except Exception:
                db_connection.session.rollback()
                raise
            finally:
                db_connection.session.close()
