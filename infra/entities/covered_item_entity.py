from sqlalchemy import Column, String, BigInteger, Integer, Date, Sequence, DateTime
from infra.config import Base


class CoveredItemEntity(Base):
    __tablename__ = "ITEMCOBERTO"
    id_seq = Sequence('ID_ITEMCOBERTO', metadata=Base.metadata)
    id_itemcoberto = Column("ID_ITEMCOBERTO", BigInteger, id_seq, nullable=False, primary_key=True)
    id_plano = Column("ID_PLANO", Integer, nullable=False)
    id_statusitemcoberto = Column("ID_STATUSITEMCOBERTO", Integer, nullable=False)
    apolice = Column("APOLICE", String, nullable=False)
    datainiciovigencia = Column("DATAINICIOVIGENCIA", Date, nullable=False)
    apoliceitem = Column("APOLICEITEM", String)
    datafimvigencia = Column("DATAFIMVIGENCIA", Date, nullable=False)
    numerocartao = Column("NUMEROCARTAO", String)
    dataprocessamento = Column("DATAPROCESSAMENTO", DateTime, nullable=False)
    datacadastro = Column("DATACADASTRO", Date, nullable=False)
    datacancelamento = Column("DATACANCELAMENTO", Date)
    datainiciocobranca = Column("DATAINICIOCOBRANCA", Date, nullable=False)
    datafimcobranca = Column("DATAFIMCOBRANCA", Date, nullable=False)
    qtddiasendosso = Column("QTDDIASENDOSSO", Integer)
    id_statusprovisorio = Column("ID_STATUSPROVISORIO", Integer)
    datastatusprovisorio = Column("DATASTATUSPROVISORIO", Date)
    id_clientecorporativo = Column("ID_CLIENTECORPORATIVO", Integer)
    datareativacao = Column("DATAREATIVACAO", Date)
    lote = Column("LOTE", Integer)
    data_lote = Column("DATA_LOTE", Date)
    id_estrutura = Column("ID_ESTRUTURA", Integer)
    data_expurgo = Column("DATA_EXPURGO", Date)
    matricula = Column("MATRICULA", String)
    codigosusep = Column("CODIGOSUSEP", Integer)
    blanket = Column("BLANKET", String)
    id_sucursal = Column("ID_SUCURSAL", Integer)
