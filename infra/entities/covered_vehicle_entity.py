from sqlalchemy import Column, String, Integer
from infra.config import Base


class CoveredVehicleEntity(Base):
    __tablename__ = "VEICULOCOBERTO"
    id_veiculocoberto = Column("ID_VEICULOCOBERTO", Integer, primary_key=True)
    marca = Column("MARCA", String)
    modelo = Column("MODELO", String)
    anofabric = Column("ANOFABRIC", Integer)
    anomodelo = Column("ANOMODELO", Integer)
    placa = Column("PLACA", String)
    chassi = Column("CHASSI", String)
    cor = Column("COR", String)
    combustivel = Column("COMBUSTIVEL", String)
    flagzerokm = Column("FLAGZEROKM", String)
    valormercado = Column("VALORMERCADO", Integer)
    flaggaragem = Column("FLAGGARAGEM", String)
    placanumerica = Column("PLACANUMERICA", String)
    id_clientecorporativo = Column("ID_CLIENTECORPORATIVO", Integer)
