from sqlalchemy import Column, String, Integer
from infra.config import Base


class ParticularityTypeEntity(Base):
    __tablename__ = "TIPOPARTICULARIDADE"

    id_tipo_particularidade = Column("ID_TIPOPARTICULARIDADE",  primary_key=True)
    descricao = Column("DESCRICAO", String, nullable=False)
    tipo_valor_partic = Column("TIPOVALORPARTIC", String, nullable=False)