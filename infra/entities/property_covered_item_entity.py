from sqlalchemy import Column, String, Integer
from infra.config import Base


class PropertyCoveredItemEntity(Base):
    """Property Covered Entity"""

    __tablename__ = "PROPRITEMCOBERTO"

    id_propritemcoberto = Column("ID_PROPRITEMCOBERTO", Integer, primary_key=True, nullable=False)
    nomeproprietario = Column("NOMEPROPRIETARIO", String)
    paisendpropr = Column("PAISENDPROPR", String)
    nomeproprietariosoundex = Column("NOMEPROPRIETARIOSOUNDEX", String)
    cnpjcpfpropr = Column("CNPJCPFPROPR", String)
    ufendpropr = Column("UFENDPROPR", String)
    cidadeendpropr = Column("CIDADEENDPROPR", String)
    bairroendpropr = Column("BAIRROENDPROPR", String)
    cependpropr = Column("CEPENDPROPR", String)
    logradouroendpropr = Column("LOGRADOUROENDPROPR", String(60))
    numeroendpropr = Column("NUMEROENDPROPR", String)
    complementoendpropr = Column("COMPLEMENTOENDPROPR", String(200))
    ddifonepropr = Column("DDIFONEPROPR", String)
    dddfonepropr = Column("DDDFONEPROPR", String)
    fonepropr = Column("FONEPROPR", String)
    id_clientecorporativo = Column("ID_CLIENTECORPORATIVO", Integer)
    cnpjcpfproprpesquisa = Column("CNPJCPFPROPRPESQUISA", Integer)

