import configparser
import functools
import json
import os
import sys

import pandas as pd
import pika
import datetime
import logging.config
from kink import di
from pyreadstat import pyreadstat
from multiprocessing import cpu_count, Pool

from container import Container
from date_format_enum import DateFormatEnum
from domain.usecases import SaveLogInputUseCase, LoadInformationUseCase, ReadFileUseCase, BuildUpdateUseCase, \
    BuildInsertCoveredItemUseCase, ExecuteInsertUpdate, BuildPropertyCoveredItemUseCase, ParticularityUseCase, \
    BuildCoveredVehicleUseCase, PropertyCoveredUseCase, BuildCoveredLifeUseCase
from domain.usecases.validation.impl.date_validation import DateValidate
from domain.usecases.validation.impl.necessary_field import NecessaryField
from domain.usecases.validation.impl.plan_validation import PlanValidation
from mult_process_fuctions import thread_property_covered_items, thread_covered_vehicles, thread_covered_life, \
    thread_property_covered, thread_particularities

from tools import get_resource_ini, send_queue_protocol, get_logger_config

logging.config.fileConfig(fname=get_logger_config(), disable_existing_loggers=False,
                          defaults={"logfilename": "mult_process.log"})

LOGGER = logging.getLogger(__name__)


def callback_consumers(conn, ch, delivery_tag, body):
    LOGGER.info('Registration processing started at %s ', datetime.date.today)
    str_folder = body['file']
    path_register_sav = os.path.join(str_folder, 'mult_process.sav')
    try:
        os.chdir(str_folder)
        os.chdir('../')
    except FileNotFoundError as error:
        LOGGER.error(error.strerror)
        sys.exit()
    path_structure_sav = os.path.join(os.getcwd(), 'structure.sav')
    contract_id = body['contract_id']
    structure_id = body['structure_id']
    client_id = body['client']
    id_log = body['id_log']
    log_input = di[SaveLogInputUseCase]
    input_log_model = log_input.find_by_id(id_log)
    LOGGER.debug("Log file founded, id_log %s", id_log)
    di["id_log"] = id_log
    di["path"] = os.getcwd()
    step_load_information(path_register_sav, path_structure_sav, contract_id, structure_id, client_id, input_log_model,
                          ch, delivery_tag)


def step_load_information(path_register_sav, path_structure_sav, contract_id, structure, client_id, input_log_model, ch,
                          delivery_tag):
    LOGGER.info("step_load_information")
    load_info = di[LoadInformationUseCase]
    plans_by_contract = load_info.load_information_plan_by_contract(contract_id)
    contract_by_id = load_info.load_contract_by_id(contract_id)
    structure_by_id = load_info.load_structure_by_id(structure)
    type_particularity = load_info.load_type_particularity()
    step_find_files(path_register_sav=path_register_sav, path_structure_sav=path_structure_sav,
                    contract_by_id=contract_by_id, plans_by_contract=plans_by_contract,
                    structure_by_id=structure_by_id, client_id=client_id, type_particularity=type_particularity,
                    input_log_model=input_log_model, ch=ch, delivery_tag=delivery_tag)


def step_find_files(path_register_sav, path_structure_sav, plans_by_contract, contract_by_id, structure_by_id,
                    client_id, type_particularity, input_log_model, ch, delivery_tag):
    LOGGER.info("step_find_files")
    read_file_use_case = ReadFileUseCase(path_register_sav)
    df_registers = read_file_use_case.reader_file()
    read_file_use_case.set_path(path_structure_sav)
    df_structure = read_file_use_case.reader_file()
    step_validation_register(contract_by_id=contract_by_id, structure_data_frame=df_structure,
                             register_data_frame=df_registers,
                             plans_by_contract=plans_by_contract,
                             structure=structure_by_id, client_id=client_id,
                             type_particularity=type_particularity, input_log_model=input_log_model, ch=ch,
                             delivery_tag=delivery_tag)


def step_validation_register(contract_by_id, structure_data_frame, register_data_frame, plans_by_contract, structure,
                             client_id, type_particularity, input_log_model, ch, delivery_tag):
    LOGGER.info("step_validation_register")
    format_str = DateFormatEnum.__members__[structure.formatodata.replace("/", "")].value

    register_data_frame = NecessaryField(data_frame_register=register_data_frame,
                                         data_frame_structure=structure_data_frame).build_validation()

    register_data_frame = PlanValidation(data_frame=register_data_frame, plans=plans_by_contract).build_validation()
    try:
        register_data_frame = DateValidate(data_frame_register=register_data_frame,
                                           contract=contract_by_id, format_str=format_str).build_validation()
    except Exception as error:
        LOGGER.error(error)
        send_protocol_queue(ch, delivery_tag, register_data_frame)
        return

    step_validation_structure(structure=structure, contract_by_id=contract_by_id,
                              register_data_frame=register_data_frame, client_id=client_id,
                              structure_data_frame=structure_data_frame, type_particularity=type_particularity,
                              date_format=format_str, input_log_model=input_log_model, ch=ch, delivery_tag=delivery_tag)


def step_validation_structure(structure, contract_by_id, client_id, register_data_frame, structure_data_frame,
                              type_particularity, date_format, input_log_model, ch, delivery_tag):
    LOGGER.info("step_validation_structure")
    date_contract = contract_by_id.datavalidade
    load_info = di[LoadInformationUseCase]

    if 'JOB_TO_EXECUTE' not in register_data_frame.columns:
        register_data_frame['JOB_TO_EXECUTE'] = None

    if 'id_itemcoberto' not in register_data_frame.columns:
        register_data_frame['id_itemcoberto'] = None

    if structure.inclusao_automatica == 'S':
        for index, row in register_data_frame.query("MESSAGE_ERROR.isnull()").iterrows():

            covered_item = load_info.load_covered_item(client_id, contract_by_id.id_contrato, row.APOLICE,
                                                       row.APOLICEITEM,
                                                       row.NUMEROCARTAO,
                                                       row.PLANO)
            data_frame_covered_item = pd.DataFrame(data=covered_item)

            if data_frame_covered_item.size > 0:
                row.JOB_TO_EXECUTE = "A"
                register_data_frame.at[index, 'JOB_TO_EXECUTE'] = "A"
            else:
                row.JOB_TO_EXECUTE = "I"
                register_data_frame.at[index, 'JOB_TO_EXECUTE'] = "I"

            row_dataframe = pd.DataFrame(data=[row.tolist()], columns=register_data_frame.columns.tolist())
            step_build_insert_update(row_dataframe, client_id, structure.id_estrutura, date_contract,
                                     data_frame_covered_item, structure_data_frame=structure_data_frame,
                                     type_particularity=type_particularity, date_format=date_format,
                                     input_log_model=input_log_model, job=row.JOB_TO_EXECUTE, index=index,
                                     register_data_frame_original=register_data_frame)

    build_complement_information(register_data_frame.query("MESSAGE_ERROR.isnull()"), structure_data_frame, client_id,
                                 type_particularity, date_format)

    send_protocol_queue(ch, delivery_tag, register_data_frame)


def send_protocol_queue(ch, delivery_tag, register_data_frame):
    LOGGER.info("send_protocol_queue")
    path = di["path"]
    pyreadstat.write_sav(register_data_frame, path + "/logger/mult_process.sav")
    payload = {
        "file": path + "/logger/mult_process.sav",
        "id_log": di["id_log"]
    }
    send_queue_protocol(payload)
    ack_message(ch, delivery_tag)
    LOGGER.info("FINISHED, %s", datetime.datetime.now())


def step_build_insert_update(register_data_frame, id_client, id_structure, date_contract, data_frame_covered_item,
                             structure_data_frame, type_particularity, date_format, input_log_model, job, index,
                             register_data_frame_original):
    LOGGER.info("step_build_insert_update")
    update_built = None
    insert_built = None
    if job == 'A':
        build_update = BuildUpdateUseCase(data_frame_covered_item=data_frame_covered_item, id_client=id_client,
                                          data_frame_register=register_data_frame, id_structure=id_structure,
                                          date_contract=date_contract, date_format=date_format,
                                          structure_data_frame=structure_data_frame)
        update_built = build_update.build_update()

    else:
        build_insert = BuildInsertCoveredItemUseCase(data_frame_register=register_data_frame, id_client=id_client,
                                                     id_structure=id_structure,
                                                     date_contract=date_contract, date_format=date_format,
                                                     structure_data_frame=structure_data_frame)
        insert_built = build_insert.build()

    step_execute_register(insert_built, update_built,
                          index,
                          register_data_frame_original)


def step_execute_register(insert_built, update_built,
                          index,
                          register_data_frame_original):
    LOGGER.info("step_execute_register")
    execute_insert_update = di[ExecuteInsertUpdate]
    if update_built is not None and len(update_built) > 0:
        record_update = execute_insert_update.execute_update(update_built)
        register_data_frame_original.at[index, "id_itemcoberto"] = int(record_update[0].id_itemcoberto)
    if insert_built is not None and len(insert_built) > 0:
        record_insert = execute_insert_update.execute_insert(insert_built)
        register_data_frame_original.at[index, "id_itemcoberto"] = int(record_insert[0].id_itemcoberto)


def build_complement_information(register_data_frame, structure_data_frame, id_client,
                                 type_particularity, date_format):
    LOGGER.info("build_complement_information")
    build_property_covered_item = BuildPropertyCoveredItemUseCase()
    builder_particularity = ParticularityUseCase()
    builder_covered_vehicle = BuildCoveredVehicleUseCase()
    builder_property_covered = PropertyCoveredUseCase()
    build_covered_life_covered = BuildCoveredLifeUseCase(date_format=date_format)

    pool = Pool(processes=max(cpu_count() - 1, 1))

    if 'PROPRITEMCOBERTO' in structure_data_frame['tabelaDestino'].values:
        pool.apply_async(func=thread_property_covered_items, args=(build_property_covered_item, register_data_frame,
                                                                   structure_data_frame, id_client,))

    if 'VEICULOCOBERTO' in structure_data_frame['tabelaDestino'].values:
        pool.apply_async(func=thread_covered_vehicles,
                         args=(builder_covered_vehicle, register_data_frame,
                               structure_data_frame, id_client,))

    if 'VIDACOBERTA' in structure_data_frame['tabelaDestino'].values:
        pool.apply_async(func=thread_covered_life,
                         args=(build_covered_life_covered, register_data_frame,
                               structure_data_frame, id_client,))

    if 'IMOVELCOBERTO' in structure_data_frame['tabelaDestino'].values:
        pool.apply_async(func=thread_property_covered,
                         args=(builder_property_covered, register_data_frame,
                               structure_data_frame, id_client,))

    if 'PARTICULARIDADE' in structure_data_frame['tabelaDestino'].values:
        pool.apply_async(func=thread_particularities,
                         args=(builder_particularity, register_data_frame,
                               structure_data_frame, pd.DataFrame(type_particularity),))

    pool.close()
    pool.join()


def ack_message(ch, delivery_tag):
    """Note that `ch` must be the same pika channel instance via which
    the message being ACKed was retrieved (AMQP protocol constraint).
    """
    if ch.is_open:
        ch.basic_ack(delivery_tag)
    else:
        # Channel is already closed, so we can't ACK this message;
        # log and/or do something that makes sense for your app in this case.
        pass


def do_work(conn, ch, delivery_tag, body):
    obj = json.loads(body.decode('UTF-8'))
    LOGGER.info(" [x] Start Process - Mult-Process %r" % obj)
    callback_consumers(conn, ch, delivery_tag, obj)


def on_message(ch, method_frame, _header_frame, body, args):
    (conn, callback_consumers) = args
    delivery_tag = method_frame.delivery_tag
    do_work(conn, ch, delivery_tag, body)


if __name__ == '__main__':

    Container()
    config = configparser.ConfigParser()
    config.read(get_resource_ini())
    host_ = config["queue"]["HOST"]
    user = config["queue"]["USER"]
    password = config["queue"]["PASSWORD"]
    port_ = int(config["queue"]["PORT"])
    credentials = pika.PlainCredentials(user, password)

    parameters = pika.ConnectionParameters(
        host=host_, port=port_, credentials=credentials, heartbeat=0)
    connection = pika.BlockingConnection(parameters)

    channel = connection.channel()
    on_message_callback = functools.partial(on_message, args=(connection, do_work))
    channel.basic_consume('process.mult_process', on_message_callback)

    try:
        channel.start_consuming()
    except KeyboardInterrupt:
        channel.stop_consuming()

    connection.close()
