from kink import inject

from infra.repositories import ParticularityRepository


@inject()
class ParticularityDataUseCase:

    def __init__(self, repository: ParticularityRepository):
        self.repository = repository

    def find_by_list_covered_item(self, covered_item: []):
        entity = self.repository.find_list_id_covered_item(covered_item)
        return entity

    def insert(self, entities):
        if len(entities) == 0:
            raise
        return self.repository.insert(entities)

    def update(self, entities):
        return self.repository.update(entities)
