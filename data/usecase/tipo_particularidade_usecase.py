from kink import inject

from infra.repositories import ParticularityTypeRepository


@inject()
class TipoParticularidadeUseCase:

    def __init__(self, repository: ParticularityTypeRepository):
        self.repository = repository

    def find_all(self):

        try:
            return self.repository.find_all()

        except Exception:
            raise
